import Vue from "vue";
import {
  Button,
  Container,
  Header,
  Aside,
  Main,
  Image,
  Loading
} from "element-ui";

Vue.use(Button);
Vue.use(Container);
Vue.use(Header);
Vue.use(Aside);
Vue.use(Main);
Vue.use(Image);
Vue.use(Loading)