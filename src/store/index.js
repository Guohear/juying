import Vue from 'vue'
import Vuex from 'vuex'
import asideList from './asidelist.json'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    window:{
      maximized:undefined
    },
    asideList
  },
  mutations: {
    SET_WINDOW_MAXIMIZED(state,payload){
      state.window.maximized = payload
    }
  },
  actions: {
  },
  modules: {
  }
})
