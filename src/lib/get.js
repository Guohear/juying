import {
  youkuvip,
  iqiyi,
  videos
} from "./examples";
export default {
  home(timeout = 3000) {
    let prms = [];
    //优酷vip推荐页轮播图
    prms.push(youkuvip(timeout));
    //爱奇艺全网影视轮播图
    prms.push(iqiyi(timeout));
    return Promise.allSettled(prms).then((pArr) => {
      //Promise新特性比axios.all 要好用很多
      //这里接口因为要单独处理统一返回,所以不采用数组循环
      let result = [];
      pArr.forEach((p) => {
        if (p.status == "fulfilled" && p.value.length != 0) {
          result.push(...p.value);
        }
      });
      return result
    });
  },
  queryName(name, timeout = 3000) {
    videos(name, timeout)
  }
};