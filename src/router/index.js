import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/main/Home'
import Type from '../components/main/Type'
Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/type/:flag',
    name: 'Type',
    component: Type,
    meta: {
      keepAlive: true
    },
    props: true
  }
]
/* , */
const router = new VueRouter({
  routes
})
// () => import( /* webpackChunkName: "Type"*/ '../components/main/Type.vue') 
export default router